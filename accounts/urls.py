from django.conf.urls import url
from django.contrib.auth import views as auth_views
from django.views.generic import TemplateView

from . import views

# app_name = 'accounts'
urlpatterns = [
    url(r'^profile/', views.Profile.as_view(), name='profile'),
    url(r'^login/', views.login_view, name='auth_login'),
    url(r'^logout/', auth_views.logout, name='auth_logout'),

    url(r'^_face/$', views.FaceAPIView.as_view(), name='face'),
]
