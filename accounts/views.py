from django.shortcuts import render, redirect
from django.views import View
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from slacksay import slacksay

from unidecode import unidecode

from presence_logger.models import Device
from . import models

import logging

# Get an instance of a logger
logger = logging.getLogger('nucleus_hub')


def login_view(request):
    return render(request, 'accounts/login.html', {'next': request.GET['next']})


@method_decorator(login_required, name='dispatch')
class Profile(View):
    template_name = 'accounts/profile.html'

    def get(self, request):
        context = {
            'ipv4_address': request.META['REMOTE_ADDR'],
            'devices': Device.objects.filter(user=request.user),
            'profile': models.Profile.objects.get(user=request.user),

        }

        return render(request, self.template_name, context=context)


class FaceAPIView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        name = request.GET['name']
        url = request.GET['link']

        profiles = models.Profile.objects.all()

        if name == 'unknown':
            slacksay.cctv(url)
            return Response({'message': 'CCTV image sent'}, status=status.HTTP_200_OK)

        for profile in profiles:
            user = profile.user
            fullname = user.first_name + "_" + user.last_name
            fullname = unidecode(fullname)
            if fullname == name:
                profile.url_face = url
                profile.save()

                # Send to Slack
                slacksay.photo(url, fullname)

                return Response({'message': 'URL successfully saved.'}, status=status.HTTP_200_OK)

        return Response({'message': f'User {name} not found.'}, status=status.HTTP_404_NOT_FOUND)
