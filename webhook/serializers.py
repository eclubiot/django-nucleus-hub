from rest_framework import serializers

from . import serializer_objects


class MetadataSerializer(serializers.Serializer):
    intentId = serializers.CharField(required=True, max_length=100)
    webhookUsed = serializers.BooleanField()
    intentName = serializers.CharField(max_length=100)

    def create(self, validated_data):
        return serializer_objects.MetadataObject(**validated_data)

    def update(self, instance, validated_data):
        for field, value in validated_data.items():
            setattr(instance, field, value)

        return instance


class ParametersSerializer(serializers.Serializer):
    name = serializers.CharField(required=False, max_length=100)

    def create(self, validated_data):
        return serializer_objects.ParametersObject(**validated_data)

    def update(self, instance, validated_data):
        for field, value in validated_data.items():
            setattr(instance, field, value)

        return instance


class ResultSerializer(serializers.Serializer):
    metadata = MetadataSerializer(required=True)
    parameters = ParametersSerializer(required=True)

    def create(self, validated_data):
        return serializer_objects.ResultObject(**validated_data)

    def update(self, instance, validated_data):
        for field, value in validated_data.items():
            setattr(instance, field, value)

        return instance


class GoogleWebhookSerializer(serializers.Serializer):
    result = ResultSerializer(required=True)

    class Meta:
        fields = ('result',)

    def create(self, validated_data):
        return serializer_objects.GoogleWebhookObject(**validated_data)

    def update(self, instance, validated_data):
        for field, value in validated_data.items():
            setattr(instance, field, value)

        return instance

    def to_representation(self, instance):
        return instance.get_response()
