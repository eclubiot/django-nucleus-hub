def ssml_test(params):
    return {
        'speech': """   <speak>
                            Here are <say-as interpret-as="characters">SSML</say-as> samples.
                            I can pause <break time="3s"/>.
                            I can play a sound
                            <audio src="https://iot.eclubprague.com/static/hello_there.mp3">hello there</audio>
                            <audio src="https://iot.eclubprague.com/static/traitor.wav">traitor</audio>.
                            I can speak in cardinals. Your number is <say-as interpret-as="cardinal">10</say-as>.
                            Or I can speak in ordinals. You are <say-as interpret-as="ordinal">10</say-as> in line.
                            Or I can even speak in digits. The digits for ten are <say-as interpret-as="characters">10</say-as>.
                            I can also substitute phrases, like the <sub alias="World Wide Web Consortium">W3C</sub>.
                            Finally, I can speak a paragraph with two sentences.
                            <p><s>This is sentence one.</s><s>This is sentence two.</s></p>
                        </speak>""",
        'displayText':  'Here are S S M L samples. I can pause [3 second pause]. I can play a sound [audio file plays].'
                        'I can speak in cardinals. Your number is ten.'
                        'Or I can speak in ordinals. You are tenth in line.'
                        'Or I can even speak in digits. The digits for ten are one oh.'
                        'I can also substitute phrases, like the World Wide Web Consortium.'
                        'Finally, I can speak a paragraph with two sentences.'
                        'This is sentence one. This is sentence two.',
        'data': {
            'google': {
                'is_ssml': True,
            }
        }
    }
