from django.contrib.auth.models import User
from django.db.models import Q, Value
from django.db.models.functions import Concat


def get_number_of_people(params):
    present_users = User.objects.exclude(devices=None).filter(
        devices__entry__is_connected=True).distinct()

    if not present_users.exists():
        return {
            'speech': f'Nobody is currently present, the place is dark and lonely.',
            'displayText': f'Nobody is currently present, the place is dark and lonely.'
        }

    return {
        'speech': f'There are currently {len(present_users)} people.',
        'displayText': f'There are currently {len(present_users)} people.'
    }


def get_person_status(params):
    name = params['name']

    # Create virtual full_name column
    users = User.objects.annotate(full_name=Concat('first_name', Value(' '), 'last_name'))

    # Find user(s) with given name
    users = users.filter(Q(first_name__iexact=name) | Q(last_name__iexact=name) |
                         Q(full_name__iexact=name) | Q(username__iexact=name))

    # Filter connected users
    users = users.filter(devices__entry__is_connected=True).distinct()

    names = ["{} {}".format(user.first_name, user.last_name).strip() for user in users]

    msg = f'No user named "{name}" is currently present.'
    if len(users) == 1:
        msg = f'User "{names[0]}" is currently present.'
    elif len(users) > 1:
        msg = f'{len(users)} users named "{name}" are currently present:\n' + ',\n'.join(names)

    return {
        'speech': msg,
        'displayText': msg
    }


def get_list_of_present_people(params):
    present_users = User.objects.exclude(devices=None).filter(
        devices__entry__is_connected=True).distinct()

    if not present_users.exists():
        return {
            'speech': f'Nobody is currently present, the place is dark and lonely.',
            'displayText': f'Nobody is currently present, the place is dark and lonely.'
        }

    present_users = [user.first_name + " " + user.last_name for user in present_users]

    return {
        'speech': 'These people are currently present:\n' + ',\n'.join(present_users),
        'displayText': 'These people are currently present:\n' + ',\n'.join(present_users),
    }
