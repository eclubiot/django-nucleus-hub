from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^google/$', views.GoogleWebhookView.as_view(), name='google-webhook'),
]
