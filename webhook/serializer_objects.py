from webhook.handlers import presence_logger, ssml_test


class GoogleWebhookObject(object):
    """Wrapping non-model object which keep data for PresenceLoggerGoogleSerializer class."""

    def __init__(self, **kwargs):
        """Based on input arguments create attributes of class instance."""
        for field in ('result',):
            setattr(self, field, kwargs.get(field, None))

    def get_response(self):
        intents = {
            '9d63fab5-3c7a-410f-9b38-30b3f2c7a170': presence_logger.get_number_of_people,
            'd62d0d60-4490-48b3-b7da-0f0801161768': presence_logger.get_person_status,
            'bdb5c0a1-4923-46ad-ac14-20a7d8bac08e': presence_logger.get_list_of_present_people,
            '7c4d925f-b016-48d9-a367-876966f24add': ssml_test.ssml_test
        }

        intent_id = self.result['metadata']['intentId']
        params = self.result['parameters']

        if intent_id in intents:
            return intents[intent_id](params)

        return self.default_intent()

    @staticmethod
    def default_intent():
        return {
            'speech': f'IntentId not found.',
            'displayText': f'IntendId not found.'
        }


class MetadataObject(object):
    def __init__(self, **kwargs):
        """Based on input arguments create attributes of class instance."""
        for field in ('intentId', 'webhookUsed', 'intentName'):
            setattr(self, field, kwargs.get(field, None))


class ParametersObject(object):
    def __init__(self, **kwargs):
        """Based on input arguments create attributes of class instance."""
        for field in ('name'):
            setattr(self, field, kwargs.get(field, None))


class ResultObject(object):
    def __init__(self, **kwargs):
        """Based on input arguments create attributes of class instance."""
        for field in ('metadata'):
            setattr(self, field, kwargs.get(field, None))
