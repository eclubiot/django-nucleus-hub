class ActiveUsersObject(object):
    """Wrapping non-model object which keep data for PresentUsersSerializer class."""

    def __init__(self, **kwargs):
        """Based on input arguments create attributes of class instance."""
        for field in ('present_users_cnt', 'absent_users_cnt', 'present_users', 'absent_users'):
            setattr(self, field, kwargs.get(field, None))



