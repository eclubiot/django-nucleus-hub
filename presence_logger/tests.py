from django.test import TestCase
from django.urls import reverse


# Create your tests here.
class DashboardTests(TestCase):
    def test_dashboard_up(self):
        """Test whether is dashboard load properly."""
        response = self.client.get(reverse('presence_logger:dashboard'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Present users')
