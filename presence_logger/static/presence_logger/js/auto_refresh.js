var refresh_time = 5;

function select_refresh_time() {
    var check_button = $('[name|="time-select"]:checked');
    //console.log(check_button.val());

    $('[id|="time-select-label"]').removeClass('btn-primary').addClass('btn-secondary');
    check_button.parent().removeClass('btn-secondary').addClass('btn-primary');

    update_refresh_time();
}

function update_refresh_time() {
    var check_button = $('[name|="time-select"]:checked');
    refresh_time = check_button.val();
    //console.log(refresh_time);
    document.cookie = "refresh_time=" + refresh_time;
}

function update_tables() {
    setTimeout(arguments.callee, refresh_time * 1000);
    $.getJSON(
        "/presence-logger/api/v1.0/_present-users/",
        function (data) {
            $("[id|='presence-table-row']").remove();
            $.each(data.present_users, function (id, user) {
                var str = '<tr id="presence-table-row">' +
                    '<th scope="row"> <i class="material-icons" style="color:#75ff33">tag_faces</i> </th>' +
                    '<td>' + user.first_name + " " + user.last_name + '</td>' +
                    '<td>' + user.present_devices + '</td>';
                if (user.url_face) {
                    str += '<td><a href="' + user.url_face + '">Link</a></td>';
                } else {
                    str += '<td></td>'
                }
                str += '</tr>';
                $('#presence-table > tbody:last-child').append(str);
            });

            $.each(data.absent_users, function (id, user) {
                $('#presence-table > tbody:last-child').append(
                    '<tr id="presence-table-row">' +
                    '<th scope="row"> <i class="material-icons" style="color:#f44336">face</i> </th>' +
                    '<td>' + user.first_name + " " + user.last_name + '</td>' +
                    '<td></td>' +
                    '<td></td>' +
                    '</tr>'
                )
                ;
            });

        }
    );
}

function checkTime(i) {
    return (i < 10) ? "0" + i : i;
}

function start_time() {
    var today = new Date(),
        y = checkTime(today.getFullYear()),
        M = checkTime(today.getMonth()),
        d = checkTime(today.getDay()),
        h = checkTime(today.getHours()),
        m = checkTime(today.getMinutes()),
        s = checkTime(today.getSeconds());
    document.getElementById('clock').innerHTML = y + "-" + M + "-" + d + " " + h + ":" + m + ":" + s;
    setTimeout(arguments.callee, refresh_time * 1000);
}




