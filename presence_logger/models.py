from django.db import models
from django.contrib.auth.models import User


class Device(models.Model):
    """Represent device.

    Each device is identify by its MAC address and is join with maximal one user instance.

   Attributes:
       user (ForeignKey, optional): Foreign key to Django User model
       mac_address (CharField): MAC address of device
    """
    user = models.ForeignKey(User, related_name='devices', null=True, blank=True, on_delete=models.CASCADE)
    mac_address = models.CharField(max_length=17, unique=True)

    class Meta:
        ordering = ['-user']

    def __str__(self):
        """String representation of model instance."""
        return f'<User: {self.user}; MAC: {self.mac_address}>'


class Entry(models.Model):
    """Represent row in ARP table.

    Each row in ARP table is used to identify whether device was in certain time connected to the AC.
    Attributes:
        timestamp (DateTimeField): Timestamp of entry automatically generated
        is_connected(BooleanField): State of device
        device (ForeigKey): Foreign key to device instance
        ipv4_address (CharField, optional): IPv4 address for current time and device
        ipv6_address (CharField, optional): IPv6 address for current time and device
    """
    device = models.OneToOneField(Device, on_delete=models.CASCADE, primary_key=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    is_connected = models.BooleanField()
    ipv4_address = models.CharField(max_length=15, null=True, blank=True)
    ipv6_address = models.CharField(max_length=45, null=True, blank=True)

    class Meta:
        ordering = ['-timestamp']
        verbose_name_plural = 'Entries'

    def __str__(self):
        """String representation of model instance."""
        return f'<TIMESTAMP: {self.timestamp}; MAC: {self.device}; IPv4: {self.ipv4_address}>'
