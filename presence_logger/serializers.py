from django.contrib.auth.models import User
from django.utils import timezone
from django.db.models import IntegerField
from rest_framework import serializers
from . import models
from . import serializer_objects


class UserHyperlinkedSerializer(serializers.HyperlinkedModelSerializer):
    """Hyperlinked Serializer for Django User model."""
    url = serializers.HyperlinkedIdentityField(view_name='presence_logger:user-detail')
    devices = serializers.HyperlinkedRelatedField(many=True, view_name='presence_logger:device-detail',
                                                  read_only=True)

    class Meta:
        model = User
        fields = ('url', 'username', 'first_name', 'last_name', 'devices')


class DeviceHyperlinkedSerializer(serializers.HyperlinkedModelSerializer):
    """Hyperlinked Serializer for Device model."""
    url = serializers.HyperlinkedIdentityField(view_name='presence_logger:device-detail')
    user = serializers.HyperlinkedRelatedField(view_name='presence_logger:user-detail', queryset=User.objects.all(),
                                               allow_null=True, required=False)

    class Meta:
        model = models.Device
        fields = ('url', 'user')


class EntryHyperlinkedSerializer(serializers.HyperlinkedModelSerializer):
    """Hyperlinked Serializer for EntryAPIView model."""
    url = serializers.HyperlinkedIdentityField(view_name='presence_logger:entry-detail')
    device = serializers.HyperlinkedRelatedField(view_name='presence_logger:device-detail',
                                                 queryset=models.Device.objects.all())

    class Meta:
        model = models.Entry
        fields = ('url', 'timestamp', 'is_connected', 'device', 'ipv4_address', 'ipv6_address')


class EntrySerializer(serializers.Serializer):
    """Serializer for ARP table.

    Update method is not implement.

    Attributes:
        event (CharField): State of device
        mac_address (CharField): MAC address of registered device
        ipv4_address (CharField, optional): IPv4 address of device
        ipv6_address (CharField, optional): IPv6 address of device
    """
    event = serializers.CharField(max_length=10)
    mac_address = serializers.CharField(max_length=17)
    ipv4_address = serializers.CharField(max_length=15, required=False, allow_null=True)
    ipv6_address = serializers.CharField(max_length=45, required=False, allow_null=True)

    def create(self, validated_data):
        """Overriding create method.

        On create check if device with given MAC address already exist. If yes EntryAPIView is associated with
        Device instance. If no new Device is created.
        """
        device = self.get_device_object(validated_data)
        try:
            entry = models.Entry.objects.get(device=device)
            entry.timestamp = timezone.now()
            entry.is_connected = validated_data.get('event') == "connect"
            entry.ipv4_address = validated_data.get('ipv4_address')
            entry.ipv6_address = validated_data.get('ipv6_address')
            entry.save()
        except models.Entry.DoesNotExist:
            entry = models.Entry.objects.create(device=device,
                                                is_connected=validated_data.get('event') == "connect",
                                                ipv4_address=validated_data.get('ipv4_address'),
                                                ipv6_address=validated_data.get(
                                                    'ipv6_address'))
        return entry

    def update(self, instance, validated_data):
        # todo: Override update method
        pass

    def to_representation(self, instance):
        """Overriding to_presentation method.

        Don't call super method in order to avoid AttributeError.
        """
        # ret = super(EntrySerializer, self).to_representation(instance)
        ret = {
            'device': str(instance.device),
            'is_connected': instance.is_connected,
            'ipv4_address': instance.ipv4_address,
            'ipv6_address': instance.ipv6_address,
        }

        return ret

    @staticmethod
    def get_device_object(validate_data):
        """Support function for get or create Device instance."""
        device = models.Device.objects.get_or_create(mac_address=validate_data.get('mac_address'))
        return device[0]


class UserSerializer(serializers.ModelSerializer):
    """Serializer for Django User model."""
    present_devices = serializers.SerializerMethodField(required=False)
    url_face = serializers.SerializerMethodField(required=False)

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'present_devices', 'url_face')

    def get_present_devices(self, obj):
        return len(models.Device.objects.filter(user=obj).filter(entry__is_connected=True))

    def get_url_face(self, obj):
        return obj.profile.url_face


class PresentUsersSerializer(serializers.Serializer):
    """Serializer for non-model ActiveUsersObject.

    Update method is not implement.

    Attributes:
         present_users_cnt (SerializerMethodField): Number of people currently connect to AP
         absent_users_cnt (IntegerField): Number of absent people
         present_users (SerializerMethodField): List of present users
         absent_users (SerializerMethodField): List of absent users

    https://stackoverflow.com/questions/28309507/django-rest-framework-filtering-for-serializer-field
    """
    present_users_cnt = serializers.SerializerMethodField()
    absent_users_cnt = serializers.SerializerMethodField()
    present_users = serializers.SerializerMethodField()
    absent_users = serializers.SerializerMethodField()

    def create(self, validated_data):
        """Overriding create method."""
        return serializer_objects.ActiveUsersObject(**validated_data)

    def update(self, instance, validated_data):
        # todo: Override update method
        pass

    def get_present_users_cnt(self, obj):
        present_users = User.objects.exclude(devices=None).filter(
            devices__entry__is_connected=True).distinct()
        return len(present_users)

    def get_absent_users_cnt(self, obj):
        absent_users = User.objects.exclude(devices=None).exclude(devices__entry__is_connected=True).distinct()
        return len(absent_users)

    def get_present_users(self, obj):
        present_users = User.objects.exclude(devices=None).filter(
            devices__entry__is_connected=True).distinct()
        serializer = UserSerializer(present_users, many=True)
        return serializer.data

    def get_absent_users(self, obj):
        absent_users = User.objects.exclude(devices=None).exclude(devices__entry__is_connected=True).distinct()
        serializer = UserSerializer(absent_users, many=True)
        return serializer.data
