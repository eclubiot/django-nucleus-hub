from django.apps import AppConfig


class PresenceLoggerConfig(AppConfig):
    name = 'presence_logger'
