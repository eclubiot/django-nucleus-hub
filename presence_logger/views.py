from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect
from django.utils.decorators import method_decorator
from django.views import View
from django.contrib.auth.models import User
from django.db.models import Count, Case, When, IntegerField
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin

from rest_framework import status, viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated

from .models import Device, Entry
from . import serializers
from . import exception

from python_arptable import get_arp_table

import logging

# Get an instance of a logger
logger = logging.getLogger('nucleus_hub')


@method_decorator(login_required, name='dispatch')
class DashboardView(View):
    """Index view for presence_logger application."""
    template_name = 'presence_logger/dashboard.html'

    def get(self, request):
        logger.info('Something went wrong!')
        # Cookies handling
        refresh_time = request.COOKIES.get('refresh_time')
        try:
            refresh_time = int(refresh_time)
        except (ValueError, TypeError):
            refresh_time = 5

        context = {
            'present_users': User.objects.exclude(devices=None).filter(
                devices__entry__is_connected=True).annotate(
                active_devices=Count(Case(
                    When(devices__entry__is_connected=True, then=1),
                    default=0, output_field=IntegerField()
                ))),
            'absent_users': User.objects.exclude(devices=None).exclude(devices__entry__is_connected=True).distinct(),
            'refresh_times': [1, 5, 10, 15],
            'refresh_time': refresh_time

        }
        return render(request, self.template_name, context=context)


class DeviceView(LoginRequiredMixin, View):
    def get(self, request, id):
        device = get_object_or_404(Device, pk=id)

        if device.user != request.user:
            messages.error(request, "You haven't rights to to that, Sir!")
            return redirect('profile')

        device.user = None
        device.save()
        messages.success(request, "Device was successfully deleted.")

        return redirect('profile')


""" REST API views
########################################################################################################################
"""

"""
Browsable API
#############
"""


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    """APIView for list User instances."""
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated,)
    queryset = User.objects.all()
    serializer_class = serializers.UserHyperlinkedSerializer


class TrackedDeviceViewSet(viewsets.ReadOnlyModelViewSet):
    """APIView for list Device instances."""
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated,)
    queryset = Device.objects.all()
    serializer_class = serializers.DeviceHyperlinkedSerializer


class EntryViewSet(viewsets.ReadOnlyModelViewSet):
    """APIView for list EntryAPIView instances."""
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated,)
    queryset = Entry.objects.all()
    serializer_class = serializers.EntryHyperlinkedSerializer


"""
Regular API
###########
"""


class EntryAPIView(APIView):
    """APIView for receive data from turris."""
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        serializer = serializers.EntrySerializer(data=request.data, many=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, format=None):
        entries = Entry.objects.all()

        for entry in entries:
            entry.is_connected = False
            entry.save()

        return Response({"status": "deleted"}, status=status.HTTP_200_OK)


class PresentUsersAPIView(APIView):
    """APIView for get active user. Used by javascript."""

    def get(self, request):
        # In parameters must be dictionary, otherwise empty dictionary will be return
        serializer = serializers.PresentUsersSerializer({})
        return Response(serializer.data)


class DeviceRegistrationView(APIView):
    """APIView for registration new devices for user. Do not use permission_classes nor authentication_classes."""

    def get(self, request, id):
        try:
            mac_address = self.get_mac_address(request)
        except exception.MACAddressError:
            return Response({
                "message": "Can't resolve MAC address. Please check if you are connected to the Wi-Fi <b>Turris</b>.",
                "class": "alert-danger",
            }, status=status.HTTP_400_BAD_REQUEST)

        try:
            user = User.objects.get(pk=id)
        except User.DoesNotExist:
            return Response({
                "message": "User with this `id` doesn't exists.",
                "class": "alert-danger",
            }, status=status.HTTP_400_BAD_REQUEST)

        try:
            device = Device.objects.get(mac_address=mac_address)
            if device.user is not None:
                return Response({
                    "message": "This device is already taken.",
                    "class": "alert-info",
                }, status=status.HTTP_400_BAD_REQUEST)

            device.user = user
        except Device.DoesNotExist:
            device = Device.objects.create(mac_address=mac_address, user=user)

        device.save()

        return Response({
            "message": "Device was successfully registered.",
            "class": "alert-success",
        }, status=status.HTTP_200_OK)

    @classmethod
    def get_mac_address(cls, request):
        mac_address = None
        ipv4_address = request.META['REMOTE_ADDR']

        for entry in get_arp_table():
            if entry["IP address"] == ipv4_address:
                mac_address = entry["HW address"]

        if mac_address is None:
            raise exception.MACAddressError()

        return mac_address
