from django.conf.urls import url, include
from . import views
from rest_framework.routers import DefaultRouter
from django.views.generic import TemplateView

# Create a router and register our viewsets with it.
router = DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'devices', views.TrackedDeviceViewSet)
router.register(r'entries', views.EntryViewSet)

app_name = 'presence_logger'
urlpatterns = [
    url(r'^dashboard/', views.DashboardView.as_view(), name='dashboard'),
    url(r'^about/', TemplateView.as_view(template_name="presence_logger/about.html"), name='about'),
    url(r'^device/delete/(?P<id>\d+)/$', views.DeviceView.as_view(), name='device-delete'),
    url(r'^device/registration/(?P<id>\d+)/$', views.DeviceRegistrationView.as_view(), name='device-registration'),

    url(r'^api/v1.0/_turris/$', views.EntryAPIView.as_view(), name='entry'),
    url(r'^api/v1.0/_present-users/$', views.PresentUsersAPIView.as_view(), name='present-user'),

    url(r'^api/v1.0/', include(router.urls)),
]
