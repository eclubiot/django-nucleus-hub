import requests

API = 'https://hooks.slack.com/services/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'


def send(data):
    headers = {'Content-type': 'application/json'}
    try:
        response = requests.post(API, headers=headers, json=data)
    # except requests.exceptions.ConnectionError:
    except:
        return False

    code = response.status_code
    if code != 200:
        return False
    else:
        return True


def text(text):
    send({"text": text})


def photo(photo_link, name):
    attachment = {
        "fallback": f"{name} in front of the camera.",
        "color": "good",
        "pretext": "Look who's at the camera.",
        "author_name": "eClubCamera",
        "text": f"Recognized as: {name}",
        "image_url": photo_link,
    }
    root = {"channel": "#camera", "attachments": [attachment]}
    return send(root)


def cctv(photo_link):
    attachment = {
        "fallback": "cctv picture",
        "color": "warning",
        "pretext": "Caught by the CCTV",
        "author_name": "eClubCamera",
        "image_url": photo_link,
    }
    # root = {"channel": "#cctv", "attachments" : [attachment]}
    root = {}
    root["attachments"] = [attachment]
    return send(root)


def warning(text):
    attachment = {
        "fallback": "WARNING",
        "color": "danger",
        "title": "Warning",
        "text": text
    }
    root = {}
    root["attachments"] = [attachment]
    return send(root)


if __name__ == "__main__":
    text("This is a text.")
    warning("This is a warning! An app has fucked up!")
    photo("http://i1.memy.pl/obrazki/535d958_no_kurwa_dawaj.jpg", "Kurwa Dawaj")
    cctv("http://i1.memy.pl/obrazki/535d958_no_kurwa_dawaj.jpg")
