# Nucleus Hub
Nucleus Hub is backend application for IoT written in Django framework.

## Local installation
1. Git clone
2. Create virtual environment and install requirements
3. Add environment variable DJANGO_SETTINGS_MODULE=nucleus_hub.settings.local
4. Run: python manage.py migrate
5. Run: python manage.py runserver

## Application(s)
1. **presence_logger** - backend for presence-logger android application
2. **webhook** - backend for API.ai
3. **accounts** - Google OAuth and Profile page

## JSON format
### Registration format
**URI:** `/presence-logger/api/v1.0/_registration`
```json
{
    "first_name": "test",
    "last_name": "test",
    "email": "test@test.com",
    "mac_address": "AA:AA:AA:AA:AA:AA"
}
```
### ARP format
**URI:** `/presence-logger/api/v1.0/_arptable`
```json
[
  {
    "event": "disconnect",
    "mac_address": "AA:AA:AA:AA:AA:AA",
    "ipv4_address": "192.168.0.1",
    "ipv6_address": "::1"
  },
  {
    "event": "connect",
    "mac_address": "AA:AA:AA:AA:AA:AB",
    "ipv4_address": "192.168.0.2",
    "ipv6_address": "::2"
  }
]
```


